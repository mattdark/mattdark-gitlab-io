---
title: "About"
date: 2022-03-08
layout: "simple"
sharingLinks: false
---

# Who Am I?

## Hi there!, I'm Mario García

I'm a developer and speaker based in Mexico. I've been an Open Source contributor over the last 15 years, and most of my work/contributions have been in localization, evangelism, content creation and software development.

Wrote my first lines of code when I was in high school, and that’s when I took an interest in software development. These days, I'm focused on improving my development skills with Python. But I also have some experience with Rust.

Was an active [Mozilla](https://mozilla.org) contributor for 9 years. I was part of the Firefox OS launch team for Mexico. Contributed to the localization of Firefox and Webmaker into Spanish. Promoted Rust across Latin America.

Also had the opportunity to join [Mozilla Open Leaders](https://foundation.mozilla.org/en/opportunity/mozilla-open-leaders/) as mentor, facilitator and featured expert from 2018 to 2020.

Due to my contributions, I also joined the [GitLab Heroes](https://about.gitlab.com/community/heroes/), and [GitKraken Ambassadors](https://www.gitkraken.com/ambassador) programs in 2019. And I've been recognized as a [HashiCorp Ambassador](https://www.hashicorp.com/ambassadors) for three years in a row since 2021.

From July to October 2021, I joined [TerminusDB](https://terminusdb.com/) as an intern, member of the Community team, where I worked on written tutorials covering how to configure the development environment, schema creation, usage of the Python client and other tools to import data to TerminusDB.

From July 2022 to May 2023, I joined [Percona](https://percona.com) as a Technical Evangelist, member of the community team, where I wrote articles on topics related to databases, containers, software development and DevOps, as well as hosting two live streams about databases, and speaking at some tech events.

I am a speaker with 15 years of experience, with a passion for teaching. Have collaborated with organizations and tech communities from around the world, and I have joined both in-person and virtual events.

As a self-taught person, I really enjoy learning new stuff. I spent the last year improving my knowledge about databases, containers and Python, and getting to know new technologies like Kubernetes, MongoDB, and some AWS tools and services.