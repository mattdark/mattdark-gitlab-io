---
title: "Speaking"
date: 2024-11-11
layout: "simple"
sharingLinks: false
---

Been a tech speaker since 2008 when I first attended my first speaking engagement, the Latin American Free Software Installfest celebrated in my city. I was invited to share my experience using Linux.

Through those years, I was an advocate for different organizations like [Mozilla](https://www.mozilla.org/), [GitLab](https://about.gitlab.com/), [GitKraken](https://www.gitkraken.com/), [HashiCorp](https://www.hashicorp.com/) and [Percona](https://www.percona.com/). I've been sharing my experience on different topics, including Open Source, Software Development, DevOps, Databases and Containers.

GitLab Commit, PyCon Chile, Pyjamas Conf, HashiTalks, FOSDEM and FOSSCOMM are some of the events I've joined as a speaker, both virtually and in-person.

I've also been a mentor for other speakers at [PyCon US](https://us.pycon.org/) and [EuroPython](https://www.europython.eu).

Below there's a playlist with some recordings available, in both Spanish and English.

{{< youtubepl id="PLj0IM88rjwPAu7ObZlNsNM6SL9r6u-x4K" >}}

Really enjoy sharing knowledge with the global community. Is there any event you want me to join? I would definitely say yes to virtual or local events. I'm available for traveling, but travel expenses are required.

Looking to participate at your first speaking engagement or need some guidance on preparing your presentation or writing a proposal? More than happy to help. Available for mentorship sessions.

Also wrote a blog post on [My Rules for Being a Tech Speaker](https://dev.to/mattdark/my-rules-for-being-a-tech-speaker-4g4o) with my recommendations and everything I've learned after being a speaker for 16 years.

{{< button href="mailto:hi@mariog.xyz" target="_self" >}}
Say Hello
{{< /button >}}