---
title: "Creating a Kubernetes Cluster on Amazon EKS With eksctl"
date: 2022-09-13T14:21:41-06:00
externalUrl: "https://percona.community/blog/2022/09/13/creating-a-kubernetes-cluster-on-amazon-eks-with-eksctl/"
summary: ""
tags: ["percona", "kubernetes", "aws", "community"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
