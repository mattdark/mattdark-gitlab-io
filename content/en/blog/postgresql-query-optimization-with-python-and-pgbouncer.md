---
title: "PostgreSQL: Query Optimization With Python and PgBouncer"
date: 2023-04-25T13:16:04-06:00
externalUrl: "https://percona.community/blog/2023/04/25/postgresql-query-optimization-with-python-and-pgbouncer/"
summary: ""
tags: ["percona", "postgresql", "python", "community"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
