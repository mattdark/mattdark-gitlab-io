---
title: "Setting Up PMM for Monitoring Your Databases on Windows"
date: 2023-01-16T14:10:02-06:00
externalUrl: "https://percona.community/blog/2023/01/16/setting-up-pmm-for-monitoring-your-databases-on-windows/"
summary: ""
tags: ["percona", "database", "monitoring", "community"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
