---
title: "Reducing PostgreSQL Costs in the Cloud"
date: 2023-02-24T14:47:34-06:00
externalUrl: "https://www.percona.com/blog/reducing-postgresql-costs-in-the-cloud/"
summary: ""
tags: ["percona", "postgresql"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
