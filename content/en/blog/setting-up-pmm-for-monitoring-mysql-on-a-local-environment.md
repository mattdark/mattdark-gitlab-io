---
title: "Setting Up PMM for Monitoring MySQL on a Local Environment"
date: 2022-08-05T14:35:51-06:00
externalUrl: "https://percona.community/blog/2022/08/05/setting-up-pmm-for-monitoring-mysql-on-a-local-environment/"
summary: ""
tags: ["percona", "mysql", "monitoring", "community"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
