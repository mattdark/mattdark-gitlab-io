---
title: "Deploy Percona Monitoring and Management on Amazon EKS With eksctl and Helm"
date: 2023-03-22T14:49:30-06:00
externalUrl: "https://www.percona.com/blog/deploy-percona-monitoring-and-management-on-amazon-eks-with-eksctl-and-helm/"
summary: ""
tags: ["percona", "monitoring", "aws", "helm"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
