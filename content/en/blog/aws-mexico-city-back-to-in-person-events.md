---
title: "AWS Mexico City: Back to In-Person Events"
date: 2022-10-19T14:15:55-06:00
externalUrl: "https://percona.community/blog/2022/10/19/aws-summit-mexico-city-back-to-in-person-events/"
summary: ""
tags: ["percona", "aws", "conference", "community"]
showReadingTime: false
_build:
  render: "false"
  list: "local"
---
