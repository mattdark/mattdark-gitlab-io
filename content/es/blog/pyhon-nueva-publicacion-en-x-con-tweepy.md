---
title: "Python: Nueva Publicación en X con Tweepy"
date: 2023-10-02
description: "Cómo publicar en X usando Python"
summary: "Aprende cómo usar Python para publicar actualizaciones en X"
tags: ["tutorial","python"]
---

Hay algunas bibliotecas de Python que pueden usarse para tener acceso a la API de X. Previamente usé [twitter](https://pypi.org/project/twitter/) para un projecto que está documentado [aquí](dev.to/mattdark/automate-your-posting-process-on-dev-with-python-bash-and-gitlab-ci-5fm6) y puede encontrarse en este [repository](https://gitlab.com/mattdark/dev-blog-posting/) de GitLab. 

Debido a los cambios recientes en la API de X, y que la biblioteca no ha sido actualizada en un año, obtenía este error cada que se ejecutaba el script que había creado:

```bash
details: {'errors': [{'message': 'You currently have access to a subset of Twitter API v2 endpoints and limited v1.1 endpoints (e.g. media post, oauth) only. If you need access to this endpoint, you may need a different access level. You can learn more here: https://developer.twitter.com/en/portal/product', 'code': 453}]}
```

La solución fue usar [Tweepy](https://github.com/tweepy/tweepy) en su lugar, una biblioteca de Python que está muy bien documentada y preparada para usarse con la version 2 de la API de X.

A través de este artículo, aprenderás como usar Tweepy para publicar actualizaciones en X.

## Crea una aplicación en el Portal de Desarrolladores de X
Si no tienes una cuenta de desarrollo, debes crear una, para poder usar la API de X. Tienes que [aplicar](https://developer.twitter.com/en/apply-for-access) para obtener acceso.

Una vez que tu cuenta de desarrollo es aprobada, visita [developer.twitter.com/en/portal/dashboard](https://developer.twitter.com/en/portal/dashboard) y [crea una aplicación nueva](https://developer.twitter.com/en/portal/apps/new), esto generará la API Key y Secret, y un Access Token y Secret para tu aplicación.

## Installation
Para instalar la versión más reciente de Tweepy puedes usar pip:

```bash
pip install tweepy
```

## Publicar desde Python
Hora de crear un script para publicar actualizaciones en X.

Primero, importa la biblioteca `tweepy` en tu proyecto:

```python
import tweepy
```

Luego, agrega las siguientes variables:

```python
consumer_key = '' #API Key
consumer_secret = '' #API Key Secret
access_token = ''
access_token_secret = ''
```

Inicia sesión con tus credenciales para poder usar la API de X:

```python
client = tweepy.Client(
    consumer_key=consumer_key, consumer_secret=consumer_secret,
    access_token=access_token, access_token_secret=access_token_secret
)
```

Y finalmente, crea una nueva publicación en X:

```python
text = 'Hello, World!'
client.create_tweet(text=text)
```

Este es un ejemplo básico de Tweepy. Revisa la [documentación](https://docs.tweepy.org/en/stable/) para información adicional y [ejemplos](https://docs.tweepy.org/en/stable/examples.html).