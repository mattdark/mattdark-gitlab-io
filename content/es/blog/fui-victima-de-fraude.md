---
title: "Fui Victima de Fraude"
date: 2024-11-20
description: "Fui victima de fraude, te cuento que pasó y solicito tu apoyo"
summary: "Fui victima de fraude, te cuento que pasó y solicito tu apoyo"
tags: ["community"]
---

Este es probablemente el artículo más personal que he escrito. No sé cuántos borradores escribí antes de decidirme a publicarlo, ya que era difícil hablar del tema. En las redes sociales hay muchas historias de éxito, pero a veces las cosas no salen como uno espera, y quería contarles lo que sucedió recientemente.

Septiembre fue un mes difícil, tuvimos algunos gastos adicionales y comencé a buscar proyectos freelance en los que pudiera trabajar durante mi tiempo libre.

Un día estaba revisando redes sociales y vi una publicación en Threads. Una empleada de Global Gig Hub, llamada Heatherm Coleman, estaba buscando personas de México para un proyecto de traducción. Le envié un mensaje directo por Instagram y le conté sobre mi interés en este proyecto. Ella me pidió que contactara a James Cresswell, el gerente del proyecto, a través de Telegram.

Le envié un mensaje a James y me contó sobre los proyectos disponibles. Le dije que estaba interesada en el proyecto de traducción. Se trataba de traducir las primeras 90 páginas de The Adventures of Huckleberry Finn a español. Me dio más información del proyecto. Confié en él y no pregunté nada más al respecto. El pago ofrecido realmente nos ayudaría a saldar las deudas que tenemos.

Empecé a trabajar en ello, durante mi tiempo libre. Después de dos semanas, terminé la traducción. La envié para revisión. Recibí una respuesta después de unas horas y me pidieron que me pusiera en contacto con el departamento de pagos. Compartí la información que solicitaron, luego me pidieron que me registrara en una plataforma que utilizan para procesar los pagos y que pagara algunas tarifas para registrarme y habilitar la transferencia a mi cuenta bancaria. Confié en ellos, tomé la decisión equivocada y perdí dinero.

No me sentí bien durante unos días. Me preguntaba cómo me había pasado esto, cuando solía hablar de privacidad y seguridad durante el tiempo que colaboré con Mozilla. Me sentí culpable. Todo lo que podía hacer era concentrarme en mi trabajo.

Pasó a fines de octubre y hoy decidí contarlo. Escribiré un artículo más adelante para compartir recomendaciones sobre cómo evitar ser víctima de una estafa o cualquier otro problema de seguridad, ya que no quiero que nadie más pase por esto.

## Global Gig Hub?
Dediqué tiempo a buscar más información sobre esta empresa. En su sitio, [globalgighub.com](https://globalgighub.com), se menciona que están ubicados en Nueva York, pero el dominio fue registrado por alguien en Reykjavík, Islandia, hace algunos meses, en octubre 17 de este año.

![Domain Information](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/s5ffycvasfsaduqm0l1o.png)

![Registrant Contact](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/705p5m4d9e6vx5b3uezb.png)

El sitio apenas fue construido, contiene textos de ejemplo, probablemente de la plantilla que están usando.

![Website](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/toun0q3rvldyvsur8tr6.png)

En redes sociales, tienen una cuenta en Instagram, sin actividad, y otro dominio listado.

![Instagram Account](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/oaa7ie7ga6nxgvuhxxvz.png)

Ese dominio, globalgighub.org, también fue registrado por alguien en Reykjavík, Islandia, hace algunos meses, en marzo 17 de este año.

![Domain Information](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/k67cj6bwwvpmvqsezsud.png)

![Registrant Contact](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/ni6wiufjkka0u0fh0dgz.png)

Estaba revisando la sección About en el sitio, y dediqué tiempo a buscar información sobre las personas en el equipo. Lo que encontré:

* La cuenta de Heatherm Coleman (heathermcoleman11 on Instagram), la persona con quien se supone que hablé, fue eliminada al poco tiempo.
* Además de Telegram, no hay otra forma de contactarlos, excepto un correo.
* Ellos están haciéndose pasar por las personas que están listadas como parte del equipo. Encontré el perfil de dos de ellos en Instagram y LinkedIn, y Global Gig Hub no se menciona en ningún lado.
* Hay errores de escritura en la sección Meet The Team, algunos nombres están mal escritos.

![Meet The Team Section](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/8yxmd0nr8hqp09kgxiwc.png)

La plataforma que ellos usan para procesar los pagos, The First City Trust Bank (https://thefirscitytb.online/), también tiene un dominio registrado recientemente por alguien en Islandia, el 14 de junio.

![Website](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/3rp61m4r1rgk6ymovnr2.png)

![Domain Information](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/9zn16p1crq98zze897l2.png)

![Registrant Contact](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/mlezj29lzzsxpuzdh6dq.png)

## ¿Cómo puedes apoyarme?
Nunca había pedido ayuda hasta mayo de este año, cuando mi pareja y yo adoptamos a Ares, que ahora es un gatito sano, mimado y juguetón. En aquel entonces, estaba enfermo y tuvo que estar hospitalizado unos días. Era mi primer mes en mi actual trabajo, después de estar un año sin trabajo. Me incorporé el 2 de mayo.

![Ares](https://media.licdn.com/dms/image/v2/D5622AQEdnbGxb__xBw/feedshare-shrink_800/feedshare-shrink_800/0/1730837076553?e=1735171200&v=beta&t=nSd7bD4rFDs85PDSFcUYcsv-_eNN5mkRsZrBBopaLpo)

Fue un gasto inesperado y decidimos pedir ayuda en redes sociales. No lo habríamos logrado de otra manera. Ofrecí sesiones de mentoría a cambio, sobre temas relacionados con Open Source, Desarrollo de Software, Hablar en Público o DevOps.

Recibimos $153.00 USD, como puedes ver en la publicación fijada en mi cuenta de [LinkedIn](https://linkedin.com/in/mariogmd). Y como prometí, abrí en mi agenda 15 espacios de 30 minutos para mentorías sin costo alguno. 

Si tú o alguien que conoces necesita orientación sobre cualquiera de estos temas, comparte mi [Calendly](https://calendly.com/mariogdev/30min) y siéntete libre de agendar una sesión.

Y esta vez vuelvo a pedir ayuda. No solo nos apoyaréis en esta difícil situación, sino que también apoyaréis el trabajo que estoy haciendo por la comunidad de código abierto.

He sido un colaborador activo en proyectos de código abierto durante los últimos 16 años, contribuyendo con código, localizando software, compartiendo conocimientos con la comunidad global escribiendo artículos técnicos o siendo conferencista o instructor en eventos de tecnlogía e innovación. La mayor parte del tiempo como voluntario, otras veces como parte de mi trabajo.

¿Qué he estado haciendo últimamente? He estado trabajando en una actualización para [mi sitio](https://mariog.dev). Estará disponible pronto tanto en español como en inglés, se agregaron o actualizaron las siguientes secciones:

* Experiencia: Se agregó esta sección para mostrar mi trabajo actual en Infotec como desarrollador de software y mi experiencia previa.
* Oratoria: Se agregó esta sección para mostrar mi experiencia como conferencista técnico, se incluyó una lista de reproducción de YouTube con grabaciones en español e inglés de conferencias anteriores. También puedea contactarme para futuras eventos.
* Ilustraciones: Comencé a dibujar recientemente, aprendiendo a usar Inkscape y decidí compartir mis creaciones, publicadas bajo Creative Commons y disponibles para descargar desde un repositorio de GitLab.
* Notas: Publicaré artículos sobre herramientas y tecnologías que no sean de código abierto e instrucciones para otros sistemas operativos.

Como parte del proyecto de localización de mi sitio web, revisaré, corregiré y actualizaré los artículos disponibles en mi cuenta de DEV y publicaré la traducción al español en mi sitio web. Hay 21 artículos en español hasta ahora.

Hay otros proyectos en los que estoy trabajando durante mi tiempo libre, pero se los haré saber y compartiré en las próximas semanas. También abriré más espacios en mi Calendly para sesiones de mentoría sin costo.

¿Cómo puedes apoyarme? A través de [PayPal](https://www.paypal.com/paypalme/mattdark). Apoyando mi trabajo vía [Buy Me A Coffee](https://buymeacoffee.com/letstalkoss/) o [contratándome](https://buymeacoffee.com/letstalkoss/extras) para cualquiera de los servicios que ofrezco.