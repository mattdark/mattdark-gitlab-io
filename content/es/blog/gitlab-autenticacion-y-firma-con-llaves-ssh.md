---
title: "GitLab: Autenticación y Firma con Llaves SSH"
date: 2023-12-11
description: "Configura GitLab para usar llaves SSH para autenticación y firma"
summary: "Aprende a firmar tus commits y autenticarte en GitLab con llaves SSH"
tags: ["tutorial", "gitlab"]
---

Para autenticarte en tu cuenta de GitLab deberás proporcionar tu usuario y contraseña. Si la autenticación en dos pasos está activada, un código adicional será solicitado. En ese caso, si eres usuario de la línea de comandos, debes crear un token de autenticación, que se usará como contraseña. Otra forma de autenticarte en tu cuenta es mediante llaves SSH. 

En este artículo, aprenderás a configurar tu cuenta, usar llaves SSH para autenticación y firma de commits. Estoy usando [GitLab.com](https://gitlab.com), pero debería funcionar si tienes una instancia propia.

## Crear Llaves SSH
De la [documentación](https://docs.gitlab.com/ee/user/ssh.html) de GitLab, se recomiendan las llaves [ED25519](https://ed25519.cr.yp.to/), ya que son más seguras y eficaces que las llaves RSA, de acuerdo al libro [Practical Cryptography With Go](https://leanpub.com/gocrypto/read#leanpub-auto-chapter-5-digital-signatures).

Para generar la llave, ejecuta el siguiente comando:

```bash
ssh-keygen -t ed25519 -C "<comment>"
```

El comentario es opcional, pero puedes escribir tu correo para identificar para que es esta llave.

En seguida, pedirá que especifiques la ruta y el nombre del archivo de la llave. Solo presiona `Enter` y acepta la configuración por defecto.

```bash
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
```

Y finalmente, especifica una frase de contraseña:

```bash
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```

Para información adicional, revisa la sección [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html) en la documentación de GitLab.

## Agregar la Llave SSH Pública a tu Cuenta
Se generan una llave pública y una privada, y debes agregar la llave pública a tu cuenta.

Copia el contenido de la llave pública:

```bash
xclip -sel clip < ~/.ssh/id_ed25519.pub
```

Después, agrega la llave a tu cuenta:

1. Inicia sesión en tu cuenta
2. Haz clic en tu foto de perfil
3. Dirígete a `Preferences`
4. Luego, abre la sección `SSH Keys`
5. Haz clic en `Add new key`
6. En el cuadro de texto `Key`, pega el contenido de la llave
7. En el cuadro de texto `Title`, escribe una descripción
8. Selecciona `Authentication & Signing` como tipo de uso
9. Cambia la fecha de expiración si es necesario
10. Haz clic en `Add key`

Para información adicional, revisa la sección [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html) en la documentación de GitLab.

## Validar la Autenticación
Para revisar que puedes iniciar sesión en tu cuenta, ejecuta el siguiente comando:

```
ssh -T git@gitlab.example.com
```

Reemplaza `gitlab.example.com` con la URL de tu instancia de GitLab. Te pedirá que confirmes que quieres agregar tu instancia de GitLab como host conocido, y escribir la frase de contraseña si está configurada.

## Configurar Git Para Firmar Commits Con Tu Llave
Una vez que se agrega la llave a tu cuenta, se debe configurar Git para usar SSH para firmar los commits:

```bash
git config --global gpg.format ssh
```

Y finalmente, especifica que llave usarás:

```bash
git config --global user.signingkey ~/.ssh/id_ed25519
```

Reemplazando `~/.ssh/id_ed25519` con la ruta de tu llave.

Omite la opción `--global`, si la firma de commits no se requiere para todos los repositorios en tu entorno local.

## Firmar Tus Commits
Una vez que Git está configurado, puedes firmar tus commits agregando la opción `-S` al comando `git commit`.

```bash
git commit -S -m "Commit description"
```

Puedes configurar Git para que los commits se firmen automáticamente:

```bash
git config --global commit.gpgsign true
```

Ahora puedes autenticarte y firmar tus commits usando llaves SSH. Revisa la [documentación](https://docs.gitlab.com/ee/user/project/repository/signed_commits/ssh.html) para información adicional.