---
title: "Monitorear MySQL con Prometheus y Grafana en Docker"
date: 2024-11-05
description: "Cómo desplegar Prometheus y Grafana para monitorear Docker"
summary: "Aprende a desplegar Prometheus y Grafana usando Docker para monitorear MySQL"
tags: ["tutorial", "mysql", "docker"]
---

Si quieres probar Prometheus para monitorear un servidor de MySQL, y su integración con Grafana, a través de este artículo aprenderás cómo ejecutarlo en un entorno local usando contenedores de Docker, lo que te ayudará a familiarizarte con esta herramienta antes de usarla en un entorno en producción.

## Ejecutar MySQL usando Docker
Primero, crea una red:

```
$ docker network create prom-network
```

Ejecuta el siguiente comando para tener MySQL corriendo en un contenedor:

```
$ docker run -d -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=root --network prom-network mysql:8.3.0
```

## Ejecutar MySQL Server Exporter con Docker
De la [documentación](https://prometheus.io/docs/instrumenting/exporters/#exporters-and-integrations) de Prometheus, hay un número de bibliotecas y servidores que pueden usarse para exportar métricas existentes de sistemas de terceros como las métricas de Prometheus. 

Algunas de estos exportadores son mantenidos como parte de la organización oficial de [Prometheus en GitHub](https://github.com/prometheus), están marcados como oficiales, otros son mantenidos y reciben contribuciones externamente.

Para MySQL, puedes usar MySQL Server Exporter que soporta:

* MySQL >= 5.6.
* MariaDB >= 10.3

Para instalar puedes seguir las instrucciones del [repositorio de GitHub](https://github.com/prometheus/mysqld_exporter), y ejecutar en un contenedor de Docker también.

Primero, otorga permisos de acceso a la base de datos. Inicia sesión en el servidor de MySQL y ejecuta las siguientes consultas:

```
$ mysql -u root -p -h 127.0.0.1
```

```
CREATE USER 'exporter'@'%' IDENTIFIED BY 'password' WITH MAX_USER_CONNECTIONS 3;
GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'exporter'@'%';
```

En seguida, crea el archivo de configuración (`config.my-cnf`), agregando la información de inicio de sesión:

```
[client]
user = exporter
password = password
```

Y finalmente, inicia el contenedor:

```
$ docker run -p 9104:9104 --name exporter --network prom-network -v $(pwd)/config.my-cnf:/cfg/config.my-cnf prom/mysqld-exporter:main --config.my-cnf=/cfg/config.my-cnf
```

## Ejecutar Prometheus en un contenedor
Después de configurar el exportador, ejecuta Prometheus en un contenedor, pero primero, crea el archivo de configuración (`prometheus.yml`). puedes empezar con el ejemplo de configuración básica de la [documentación](https://prometheus.io/docs/prometheus/latest/getting_started/), y agrega la configuración necesaria para el exportador como se describe en el repositorio de GitHub.

```
global:
  scrape_interval:     15s # By default, scrape targets every 15 seconds.

  # Attach these labels to any time series or alerts when communicating with
  # external systems (federation, remote storage, Alertmanager).
  external_labels:
    monitor: 'codelab-monitor'

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'prometheus'

    # Override the global default and scrape targets from this job every 5 seconds.
    scrape_interval: 5s

    static_configs:
      - targets: ['localhost:9090']

  - job_name: 'mysql'

    params:
      auth_module: [client]

    scrape_interval: 5s

    static_configs:
      - targets: ['mysql:3306']

    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
          # The mysqld_exporter host:port
        replacement: exporter:9104
```

Ahora ejecuta el contenedor:

```
$ docker run -p 9090:9090 --network prom-network --name prometheus -v ./prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus
```

Una vez que Prometheus está ejecutándose, puedes ir al navegador y visitar `http://localhost:9090` o si vas a `http://localhost:9090/targets`, puedes ver el exportador listado ahí.

![Prometheus - Targets](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/a5k5i0u3f8zbwcsmv4c6.png)

## Ejecutar Grafana con Docker
Para ejecutar Grafana con Docker, solo escribe el siguiente comando:

```
$ docker run -d --name=grafana --network prom-network -p 3000:3000 grafana/grafana-enterprise
```

Ve a `localhost:3000` en el navegador. Inicia sesión con las credenciales por defecto, `admin` como usuario y contraseña.

![Grafana - Login](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/1mf3bt8d9zxbulnsnrxr.png)

Y cambia la contraseña por defecto.

![Grafana - Update password](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/02gsfy0gg8xzmjjocn3z.png)

Desde el menú, ve a `Data sources` en la sección `Connections` o ve directamente a `http://localhost:3000/connections/datasources`.

Haz clic en `Add data source`, después en `Prometheus`, escribe `http://prometheus:9090` como la URL del servidor de Prometheus, y haz clic en `Save & test`.

Ahora ve a `Dashboards` desde el menú o ve directamente a `http://localhost:3000/dashboards`.

Haz clic en `Create Dashboard`, después en `Import dashboard`, escribe `14057`, haz clic en `Load`, y después en `Import`.

Si todo es correcto, verás la siguiente pantalla.

![Grafana - Dashboard](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/rajr3xkhceyv60no2ra2.png)

## Conclusión
Después de haber seguido los pasos anteriores, estás ejecutando un servidor de MySQL que está siendo monitoreado con Prometheus y Grafana. A través de este artículo aprendiste a ejecutar un servidor de MySQL, Prometheus y Grafana en tu entorno local usando Docker.