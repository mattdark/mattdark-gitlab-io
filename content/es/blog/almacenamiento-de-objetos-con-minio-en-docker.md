---
title: "Almacenamiento de Objetos con MinIO en Docker"
date: 2023-12-28
description: "Cómo configurar almacenamiento de objetos con MinIO usando Docker"
summary: "Aprende a desplegar una instancia de MinIO usando Docker"
tags: ["tutorial", "docker", "linux"]
series: ["Percona Backup for MongoDB en un Entorno de Desarrollo con Docker"]
series_order: 2
---

Este artículo es la segunda parte de la serie sobre Percona Backup for MongoDB (PBM) que incluye los siguientes artículos:

1. Desplegar un Clúster de MongoDB con Docker
2. Almacenamiento de Objectos con MinIO en Docker
3. Ejecutar Percona Backup for MongoDB con Docker

A través de este artículo, aprenderás como configurar MinIO y usarlo con PBM para almacenar el respaldo de tus bases de datos. Como en el artículo anterior, se usará Docker para configurar MinIO en un entorno de desarollo.

Percona Backup for MongoDB soporta los siguientes tipos de [almacenamiento de respaldo remoto](https://docs.percona.com/percona-backup-mongodb/details/storage-configuration.html):

* Almacenamiento compatible con S3
* Almacenamiento basado en el sistema de archivos
* Microsoft Azure Blob storage

PBM funciona con cualquiera de las siguientes opciones de almacenamiento compatible con S3:

* [Amazon Simple Storage Service](https://docs.aws.amazon.com/s3/index.html)
* [Google Cloud Storage](https://cloud.google.com/storage)
* [MinIO](https://min.io/)

## Almacenamiento de Objetos con MinIO
[MinIO](https://min.io/docs) es una solución de almacenamiento de objetos que proporciona una API de Amazon Web Services compatible con S3 y soporta las funcionalidades principales de S3. MinIO fue contruido para desplegarse en cualquier lugar - nube pública o privada, infrestructura baremetal, entornos de orquestación e infraestructura de borde.

Puedes instalar y configurar MinIO usando Docker y Kubernetes, así como instalarlo directamente en tu sistema operativo. ya que ofrece para Linux, Mac y Windows.

Puedes seguir las instrucciones en [la documentación](https://min.io/docs/minio/container/index.html) para ejecutarlo usando Docker.

Primero, necesitas crear un directorio local:

```
$ mkdir -p ~/minio/data
```

Este directorio servirá como volumen persistente y será usado por el contenedor.

En seguida, inicia el contenedor ejecutando el siguiente comando:

```
docker run \
   -p 9000:9000 \
   -p 9001:9001 \
   --name minio \
   --network mongodbCluster \
   -v ~/minio/data:/data \
   -e "MINIO_ROOT_USER=username" \
   -e "MINIO_ROOT_PASSWORD=password" \
   quay.io/minio/minio server /data --console-address ":9001"
```

Los parametros en el comando anterior corresponden a:

* `-p`. Los puertos en el host que recibirán y redirigirán todas las peticiones a los puertos en el contenedor
* `--name`. Nombre del contenedor
* `--network`. Nombre de la red a la que estará conectado el contendor
* `--v`. Directorio usado como volumen persistente para el contenedor
* `-e`. Define las variables de entorno `MINIO_ROOT_USER` y `MINIO_ROOT_PASSWORD`

El contenedor se construye desde el repositorio de MinIO en [Quay.io](https://quay.io/). El puerto `9000` se usa para conectarse a la API y el puerto `9001` es para acceder a la consola en el navegador.

No olvides reemplazar `username` y `password` en las variables de entorno con los detalles de autenticación. 

Esto es parte de la salida que obtienes después de inicializar el contedor:

```
Status:         1 Online, 0 Offline.
S3-API: http://172.20.0.5:9000  http://127.0.0.1:9000
Console: http://172.20.0.5:9001 http://127.0.0.1:9001
```

`http://172.20.0.5:9000` es la URL requerida para configurar PBM para que use MinIO.

Una vez que MinIO está ejecutándose, puedes acceder a la consola visitando `https://localhost:9001` en el navegador.

![MinIO UI](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/minio-ui.png)

Ahora es momento de crear el bucket para almacenar tus respaldos. Dirígete a *Bucket*, clic en *Create Bucket* y asignale un nombre (`mongo-backup`).

![Create Bucket](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/create-bucket.png?ref_type=heads)

Habilita las siguientes opciones si las necesitas:

* **Versioning** permite conservar multiples versiones del mismo objeto bajo la misma llave.
* **Object Locking** previene que los objetos sean eliminados. Requerido para soportar la convservación y retención legal. Solo puede habilitarse durante la creación del bucket.
* **Quota** limita el número de datos en el bucket.

En seguida, haz clic en el botón *Create Bucket*.

Ahora, dirígete a *Identity* → *Users* y haz clic en *Create User*.

![Create User](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/create-user.png?ref_type=heads)

Escribe los valores de *User Name* (`pbm-user`) y *Password* y asigna las políticas requeridas, al menos la política `readwrite`. Haz clic en *Save*.

El usuario fue creado. Accede a los detalles del usuario y haz clic en *Access Key*.

![User Details](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/user-details.png?ref_type=heads)

Visita la sección *Service Accounts* y haz clic en *Create Access Key*.

![Service Accounts](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/service-accounts.png?ref_type=heads)

Access Key y Secret Key serán generadas automáticamente, solo haz clic en *Create*.

![Access Key](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/access-key-created.png?ref_type=heads)

Descarga tu access key ya que necesitarás esos datos para autenticarte desde PBM.

![Download Access Key](https://gitlab.com/mattdark/blog-posts-assets/-/raw/master/pbm-docker/minio-user-access-key.png?ref_type=heads)

## Conclusión
A través de este artículo, aprendiste a configurar MinIO para almacenar los archivos de respaldo generados por Percona Backup for MongoDB.