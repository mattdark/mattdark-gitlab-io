---
title: "Git: Copia de Seguridad y Restauración de Llaves GPG y SSH"
date: 2023-12-11
description: "Cómo respaldar y restaurar llaves GPG y SSH usadas con Git"
summary: "Aprende a respaldar y restaurar llaves GPG y SSH"
tags: ["tutorial", "git", "linux"]
---

Para firmar tus commits de Git puedes usar llaves SSH (como he explicado en este [artículo](https://mariog.dev/blog/gitlab-autenticacion-y-firma-con-llaves-ssh/)) o [GPG](https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html).

En este artículo aprenderás como respaldar y restaurar tus llaves en caso de cambiar de equipo o reinstalar sistema.

## Respaldo y Restauración

### SSH

Para respaldar tus llaves:

* Copia los archivos `id_x` y `id_x.pub` del directorio `~/.ssh/` a una memoria USB

Donde `x` es cualquiera de los tipos de llaves SSH soportadas, como los que [soporta GitLab](https://docs.gitlab.com/ee/user/ssh.html#supported-ssh-key-types), de acuerdo a la siguiente tabla:

| Algoritmo                        | Llave Pública     | Llave Privada |
|----------------------------------|-------------------|---------------|
| ED25519 (preferred)              | id_ed25519.pub    | id_ed25519    |
| ED25519_SK                       | id_ed25519_sk.pub | id_ed25519_sk |
| ECDSA_SK                         | id_ecdsa_sk.pub   | id_ecdsa_sk   |
| RSA (at least 2048-bit key size) | id_rsa.pub        | id_rsa        |
| DSA (deprecated)                 | id_dsa.pub        | id_dsa        |
| ECDSA                            | id_ecdsa.pub      | id_ecdsa      |

Para restaurar tus llaves, sigue las instrucciones:

* Copia los archivos `id_x` y `id_x.pub` de la memoria USB y pégalos en el directorio `~/.ssh/`
* Cambia los permisos y propiedad de ambos archivos

```bash
$ chown user:user ~/.ssh/id_rsa*
$ chmod 600 ~/.ssh/id_rsa
$ chmod 644 ~/.ssh/id_rsa.pub
```

Donde `user` es el nombre de usuario con el que inicias sesión en el sistema

* Inicia `ssh-agent`

```bash
$ exec ssh-agent bash
```

* Agrega tu llave privada SSH a `ssh-agent`

```bash
$ ssh-add ~/.ssh/id_rsa
```

### GPG

* Identifica la llave privada ejecutando el siguiente comando:

```bash
$ gpg --list-secret-keys --keyid-format LONG
```

Mostrará lo siguiente:

```bash
sec   4096R/3AA5C34371567BD2 2016-03-10 [expires: 2017-03-10]
```

Los caracteres después de `4096R/` corresponden al ID de la llave privada.

* Exporta la llave privada

```bash
gpg --export-secret-keys $ID > my-private-key.asc
```

`$ID` es el valor del ID de tu llave privada, obtenido en el paso anterior.

* Copia el archivo `my-private-key.asc` a una memoria USB

Para restaura:

* Copia el archivo `my-private-key.asc` de la memoria USB y pégalo en el dispositivo al que deseas importarlo

* Importa tu llave GPG

```bash
$ gpg --import my-private-key.asc
```

Una vez que las llaves se han importado, podrás continuar firmando tus commits.